
#use wml::ossp area=doc:license

<title>Documentation</title>

<subtitle>Licensing and Copyrights</subtitle>

<h1>Licensing and Copyrights</h1>

The OSSP project uses three distinguished and OSI-approved <a
href="http://www.opensource.org/">Open Source</a> software distribution
licenses:

<ul>
  <li><a href="http://www.gnu.org/licenses/gpl.html"><b>GNU General Public License</b> (2.0)</a><br>
      <a href="http://www.gnu.org/licenses/gpl.html">http://www.gnu.org/licenses/gpl.html</a>
	  <p>
      This is the strongest license, meaning it applies the most
      restrictions. It is used in OSSP mainly for stand-alone tools which
      are not included (by copying) into other components.  The
	  intention is to make sure the covered software remains
      fully free software and allow others to use it mostly everywhere only
      under certain strong restrictions.
  <p> 
  <li><a href="http://www.gnu.org/licenses/lgpl.html"><b>GNU Lesser General Public License</b> (2.1)</a><br>
      <a href="http://www.gnu.org/licenses/lgpl.html">http://www.gnu.org/licenses/lgpl.html</a>
	  <p>
      This is the compromise license, meaning it applies a reasonable
      set of restrictions but without making too much trouble for the
      user. It is used in OSSP mainly for libraries which should be
      strongly protected but which are not included (by copying) into
      other libraries (as sub-components). The intention is to make sure
      the covered software remains mostly fully free software and allow
      others to use it mostly everywhere without too strong
      restrictions.
  <p>
  <li><a href="http://www.opensource.org/licenses/mit-license.php"><b>MIT-Style License</b></a><br>
	  <a href="http://www.opensource.org/licenses/mit-license.php">http://www.opensource.org/licenses/mit-license.php</a>
	  <p>
      This is the weakest license, meaning it applies the least
      restrictions. It is used in OSSP mainly for libraries which
      are potentially included (by copying) into other libraries (as
      sub-components). The intention is to make sure the covered
      software can be used really everywhere and for every reason
      without any real restrictions.
</ul>

All OSSP software components are covered by one of the three licenses
above. They clearly indicate this with an obvious license declarations
at the top of each source file. The used declarations follow:

<h2>GNU General Public License</h2>

<font size=-1><tt><pre>
#include "license-gpl.txt"
</pre></tt></font>

<h2>GNU Lesser General Public License</h2>

<font size=-1><tt><pre>
#include "license-lgpl.txt"
</pre></tt></font>

<h2>MIT License</h2>

<font size=-1><tt><pre>
#include "license-mit.txt"
</pre></tt></font>

