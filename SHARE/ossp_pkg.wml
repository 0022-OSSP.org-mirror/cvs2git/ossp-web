##
##  ossp_pkg.wml -- Package Macros
##  Copyright (c) 2000-2001 Ralf S. Engelschall <rse@engelschall.com> 
##

#   define a process bar
<define-tag pbar>
  <preserve width pdone cdone ctext ctodo/>
  <set-var %attributes/>
    <set-var ptodo=<substract 100 <get-var pdone/>/>/>
    <set-var wdone=<divide <multiply <get-var width/> <get-var pdone/>/> 100/>/>
    <set-var wtodo=<divide <multiply <get-var width/> <get-var ptodo/>/> 100/>/>
    <table cellspacing=0 cellpadding=0 border=0>
      <tr>
        <ifeq <get-var wdone/> 0 "" <group
        <td width="<get-var wdone/>" bgcolor="<get-var cdone/>" align=right>
          <imgdot width=<get-var wdone>><br>
          <font color="<get-var ctext/>" face="arial,helvetica"><b>
            <get-var pdone/>%
          </b></font>
        </td>/>/>
        <ifeq <get-var wtodo/> 0 "" <group
        <td width="<get-var wtodo/>" bgcolor="<get-var ctodo/>">
          <imgdot width=<get-var wtodo>>
        </td>/>/>
      </tr>
    </table>
  <restore width pdone cdone ctext ctodo/>
</define-tag>

#   define a macro which can be used to create a file listing which
#   is optically more compact than the stuff Apache's mod_autoindex
#   creates.
..PAGE_CSS>>
TR.files0N                            { background: #f5f5f0; }
TR.files1N                            { background: #e5e5e0; }
.files0N,.files1N,A.files0N,A.files1N { color: #000000; }
TR.files0S,TR.files1S                 { background: #993333; }
.files0S,.files1S,A.files0S,A.files1S { color: #ffffff; }
TR.files0U,TR.files1U                 { background: #666699; }
.files0U,.files1U,A.files0U,A.files1U { color: #ffffff; }
<<..
<define-tag pkg_files>
<preserve cvs url directory files stable unstable>
<set-var %attributes>
<:
my $url       = "<get-var url>";
my $directory = "<get-var directory>";
my $files     = "<get-var files>";
my $stable    = "<get-var stable>";
my $unstable  = "<get-var unstable>";
$url =~ s|/+$||s;
:>
<table width=100% cellspacing=1 cellpadding=0 border=0>
<tr>
  <td colspan=3>
    ChangeLog: <a href="<get-var cvs>ChangeLog" class=plain><get-var cvs>ChangeLog</a>
  </td>
</tr>
<tr>
  <td colspan=3>
    Repository: <a href="<get-var cvs>" class=plain><get-var cvs></a>
  </td>
</tr>
<tr>
  <td colspan=3>
    Distribution: <a href="<get-var url>" class=plain><get-var url></a><br><br>
  </td>
</tr>
<tr>
  <td><b>Distribution Files:</b></td>
  <td align=right><b>Size:</b></td>
  # <td align=right><b>MD5:</b></td>
  <td align=right><b>Time:</b></td>
</tr>
<:
my $odir = chdir($directory) || die;
my @F = reverse sort { (stat($a))[9] <=> (stat($b))[9]; } (glob($files));
chdir($odir);
my $O = '';
$ncol = 0;
foreach my $f (@F) {
    my @S = stat($f);
	my $size = $S[7];
	if ($size > (1024*1024)) {
		$size = sprintf("%.1fMB", $size / (1024*1024));
	}
	elsif ($size > (1024)) {
		$size = sprintf("%.1fKB", $size / (1024));
	}
    $f = "$f/" if (-d $f);
    my @T = localtime($S[9]);
    my @moy = ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
               'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
	$ncol = ($ncol + 1) % 2;
	my $type = "N";
	$type = "S" if ($f =~ m|$stable|);
	$type = "U" if ($f =~ m|$unstable|);
    my $class = "files${ncol}${type}";
	#my $md5 = `openssl md5 "$f"`;
	#$md5 =~ s|^MD5\(.+?\)\s*=\s*(\S+)|$1|s;
	##$md5 =~ s|^([a-zA-Z0-9]{16})([a-zA-Z0-9]{16})$|$1<br>$2|;
	#$md5 = "<font face=\"Courier,Monospace\" size=\"-6\">$md5</font>";
    $f = "<a href=\"".$url."/".$f."\" class=$class>$f</a>";
    my $e = "<tr class=$class>" .
	        "  <td width=100%>".$f."&nbsp;&nbsp;</td>" .
		    "  <td align=right>&nbsp;&nbsp;".$size."</td>" .
		    # "  <td align=right>&nbsp;&nbsp;".$md5."</td>" .
	        "  <td align=right>&nbsp;&nbsp;".sprintf("%"."02d-%"."s-%"."04d", $T[3], $moy[$T[4]], 1900+$T[5]) . "</td>".
			"</tr>";
    $O .= $e . "\n";
}
print $O;
:>
</table>
<restore cvs url directory files stable unstable>
</define-tag>

#   define a package summary list
<define-tag pkg_list endtag=required>
<table width=100% cellspacing=1 cellpadding=0 border=0>
  %body
  <tr>
    <td colspan=3 height=1 bgcolor="#b5b5b0"><imgdot height=1></td>
  </tr>
  <tr>
    <td colspan=3>
	   <b>*</b> derived from external sources
	</td>
  </tr>
</table>
</define-tag>
<define-tag pkg_item>
<preserve name longname desc done tag stable unstable derived type>
<set-var %attributes>
  <tr>
    <td colspan=3 height=1 bgcolor="#b5b5b0"><imgdot height=1></td>
  </tr>
  <tr>
    <td width=100%>
	    <a href="<get-var name>/"><b><get-var longname></b></a>
		<ifeq "<get-var derived>" "yes" "&nbsp;<b>*</b>">
	</td>
    <td>Stable:   
      <ifeq "<get-var stable>" "none" "none"
        <a href="ftp://ftp.ossp.org/pkg/<get-var type>/<get-var name>/<get-var name>-<get-var stable>.tar.gz"><get-var stable></a>
      >
	</td>
    <td>Unstable: 
	  <ifeq "<get-var unstable>" "none" "none"
	    <a href="ftp://ftp.ossp.org/pkg/<get-var type>/<get-var name>/<get-var name>-<get-var unstable>.tar.gz"><get-var unstable></a>
	  >
	</td>
  </tr>
  <tr>
    <td><get-var desc></td>
    <td colspan=2>
      <pbar width=200 pdone="<get-var done>" cdone="#b5b5b0" ctext="#f5f5f0" ctodo="#996666">
	</td>
  </tr>
<restore name longname desc done tag stable unstable derived type>
</define-tag>

<define-tag pkg_status>
<preserve genesis name assign stable unstable stable_date unstable_date done>
<set-var %attributes>
<table cellspacing=0 cellpadding=0 border=0> 
  <tr>
    <td>Stable Version:&nbsp;&nbsp;</td><td class=heavy><get-var stable></td><td>&nbsp;(<get-var stable_date>)</td>
	<td>
        <ifeq "<get-var name>" "" "" <group
          <ifeq "<get-var stable>" "none" "" <group
            &nbsp;&nbsp;[<a href="http://cvs.ossp.org/tktnew?w=<get-var assign>&s=<get-var name>&v=<get-var stable>">Bug&nbsp;Reporting</a>]
		  >>
		>>
    </td>
  </tr>
  <ifeq "<get-var unstable>" "none" "" <group
    <tr>
      <td>Unstable Version:&nbsp;&nbsp;</td><td class=heavy><get-var unstable></td><td>&nbsp;(<get-var unstable_date>)</td>
  	  <td>
          <ifeq "<get-var name>" "" "" <group
            &nbsp;&nbsp;[<a href="http://cvs.ossp.org/tktnew?w=<get-var assign>&s=<get-var name>&v=<get-var unstable>">Bug&nbsp;Reporting</a>]
	      >>
      </td>
    </tr>
  >>
  <ifeq "<get-var genesis>" "none" "" <group
    <tr>
      <td>Genesis:&nbsp;&nbsp;</td><td colspan=2><get-var genesis></td>
    </tr>
  >>
  # <ifeq "<get-var done>" "100" "" <group
    <tr>
      <td>Finished:</td><td colspan=2><pbar width=100 pdone="<get-var done>" cdone="#b5b5b0" ctext="#f5f5f0" ctodo="#996666"></td>
    </tr>
  # >>
</table>
#  <a href="ftp://ftp.ossp.org/pkg/<get-var type>/<get-var name>/<get-var name>-<get-var stable>.tar.gz"><get-var stable></a>
<restore genesis name assign stable unstable stable_date unstable_date done>
</define-tag>

<define-tag pkg_author>
<preserve name mail>
<set-var %attributes>
  <get-var name> &lt;<a href="mailto:<get-var mail>"><get-var mail></a>&gt;<br>
<restore name mail>
</define-tag>

<define-tag pkg_donation>
<preserve name return>
<set-var %attributes>
<table cellspacing=0 cellpadding=0 border=0>
<tr width=100%>
<td align=left valign=top>
<font size=-1>
OSSP <get-var name> is a sub-project of OSSP and hence a fully
non-profit <a href="http://www.opensource.org/">Open Source Software</a>
effort. The development on OSSP <a href="$(ROOT)/pkg/">projects</a>
like OSSP <get-var name> is supported by the contributions and <a
href="$(ROOT)/com/sponsors.html">sponsoring</a> of individuals
and companies. You can support OSSP <get-var name> and the
other OSSP projects yourself by contributing source fixes and
enhancements or by <a href="$(ROOT)/com/donations.html">donating</a>
money. 
To reward the efforts on OSSP <get-var name>, just
press the button to the right to perform a secure <a
href="http://www.paypal.com/">PayPal</a> online money transaction to the
OSSP project. Credit will be given to you on the OSSP <a
href="$(ROOT)/com/sponsors.html">Sponsors</a> page once your donation
was received.
</font>
</td>
<td>
&nbsp;&nbsp;&nbsp;&nbsp;
</td>
<td align=right valign=top>
<donation project="<get-var name>" return="<get-var return>">
</td>
</tr>
</table>
<restore name return>
</define-tag>

<define-tag pkg_manpage>
<preserve name sect path>
<set-var %attributes>
  <a href="$(ROOT)/man/man.cgi<get-var path>"><get-var name></a>(<get-var sect>)
<restore name sect path>
</define-tag>

##
##  Next-Generation Package Macros
##

<define-tag pkg:see-also endtag=required>
<:
    my $ref = '%body';
    my $O = '';
    if ($ref =~ m/^((https?|ftp):\/\/\S+)$/) {
        $O = "<a href=\"$ref\">$ref</a>";
    }
    elsif ($ref =~ m|^(\S+)\(([1-9])\)@(/\S+)$|) {
       $O = "<a href=\"$(ROOT)/man/man.cgi$3\">$1</a>($2)";
	}
    elsif ($ref =~ m|^(\S+)\(([1-9])\)$|) {
       $O = "<a href=\"http://www.freebsd.org/cgi/man.cgi?" .
            "query=$1&sektion=$2&manpath=FreeBSD+5.0-current&format=html\">" .
            "$1</a>($2)";
    }
    elsif ($ref =~ m|^OSSP\s+(\S+)$|) {
        my $name = $1;
        foreach my $path (qw(.. ../../lib ../../tool)) {
            if (-d "$path/$name") {
                $O = "<a href=\"$path/$name/\">$ref</a>";
                last;
            }
        }
    }
    if ($O eq '') {
        $O = $ref;
    }
    print $O;
:>
</define-tag>

<define-tag pkg:lookup endtag=required>
<preserve dict>
<set-var %attributes>
  <ifeq "<get-var dict>" "jargon"
    <a href="http://www.tf.hut.fi/cgi-bin/jargon?search=foo">foo</a>
	FIXME
  />
<restore dict>
</define-tag>

