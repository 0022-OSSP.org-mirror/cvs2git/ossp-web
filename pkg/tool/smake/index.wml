
#use wml::ossp area=pkg:tool subarea=smake

<title>OSSP smake</title>

<h1>Skeleton Makefile Generator</h1>

<h2>Abstract</h2>

OSSP smake is a powerful mechanism to generate standard Makefiles out
of skeleton Makefiles which only provide the essential parts. The
missing stuff gets automatically filled in by shared include files. A
reasonable scheme to create a huge Makefile hierarchy and to keep it
consistent for the time of development. The trick is that it merges
the skeleton (<tt>Makefile.sm</tt>) and the templates (include files)
in a priority-driven way, i.e. defines and targets can be overwritten.
The idea is taken from <a href="http://www.x.org/">X Consortium</a>'s
<tt>imake</tt>, but the goal here is not inherited system independence
for the Makefiles, the goal is consistency and power without the need of
manually maintaining a Makefile hierarchy consisting of plain Makefiles.

<h2>Authors</h2>

<pkg_author name="Ralf S. Engelschall" mail="rse@engelschall.com">

<h2>Status</h2>

<pkg_status
    stable="none"    stable_date="none"
    unstable="none"  unstable_date="none"
	done=90>

<h2>Source</h2>

<pkg_files 
    cvs=$(CVS_ROOT_URL)/pkg/tool/smake/
    url=$(FTP_ROOT_URL)/pkg/tool/smake/
    directory=$(FTP_ROOT_DIR)/pkg/tool/smake/
    files="smake-*.tar.gz" 
	stable="smake-1.3.2.tar.gz" unstable="none">
	
<h2>Donation</h2>

<pkg_donation name="smake" return="$(BASE_URL)/pkg/tool/smake/">

