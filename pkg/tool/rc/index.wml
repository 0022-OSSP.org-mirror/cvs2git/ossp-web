
#use wml::ossp area=pkg:tool subarea=rc

<title>OSSP rc</title>

<h1>Run-Command Processor</h1>

<h2>Abstract</h2>

OSSP rc is a generic run-command processor. Its primary function is to
assemble a temporary script from excerpts of one or more run-commmand
files which are built out of text snippets grouped into sections.
The user specifies the desired parts to use and also controls the
order of assembly. The program is not tied to a particular syntax for
neither the run-command section tags nor the contained scripts. By
default, the assembled script is executed by a specified interpreter
(usually a shell), but it can also be written into a temporary file
for evaluation inside a calling shell, or even just printed to stdout
for further processing.

<p>
Its primary purpose is to be used as a generic
run-command facility in an operating system or a sub-system like <a
href="http://www.openpkg.org/">OpenPKG</a>.

<h2>Documentation</h2>

<pkg_manpage name="rc" sect=1 path="/pkg/tool/rc/rc.pod">,
<pkg_manpage name="rc-sample" sect=5 path="/pkg/tool/rc/rc-sample.pod">.

<h2>Authors</h2>

<pkg_author name="Michael Schloh v. Bennewitz" mail="michael.schloh@cw.com">

<h2>Status</h2>

<pkg_status
    stable="none" stable_date="none"
    unstable="0.7.1" unstable_date="07-Jul-2003"
	done=95>

<h2>Source</h2>

<pkg_files 
    cvs=$(CVS_ROOT_URL)/pkg/tool/rc/
    url=$(FTP_ROOT_URL)/pkg/tool/rc/
    directory=$(FTP_ROOT_DIR)/pkg/tool/rc/
    files="rc-*.tar.gz" 
	stable="none" unstable="0.7.1">
	
