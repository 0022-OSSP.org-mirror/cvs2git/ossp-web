
#use wml::ossp area=pkg:lib subarea=popt

<title>OSSP popt</title>

<h1>Option Parsing</h1>

<h2>Abstract</h2>

OSSP popt is a derivative of <a href="http://www.redhat.com/">RedHat</a>'s <a
href="ftp.rpm.org:/pub/rpm/dist/rpm-4.0.x">POPT</a> library as found in the <a
href="http://www.rpm.org/">RedHat Package Manager</a> (RPM). It provides
support for parsing short (<tt>-f</tt>) and long (<tt>--foo</tt>) command line
options.  Unlike similar libraries, it does not utilize global variables, thus
enabling multiple passes in parsing argument vectors (like <tt>argv</tt>); it
can parse an arbitrary array of <tt>argv</tt>-style elements, allowing parsing
of command-line options from any source; it provides a standard method of
option aliasing; it can execute external option filters; and, finally, it can
automatically generate help and usage messages for the application.

<p>
Notice: OSSP popt is not compatible with the original POPT library, because it
uses a different API namespace. Functionality wise it is the same, although
the source code was stripped down to its minimum. It made into an OSSP library
for easier integration within the OSSP project. It is not intended to replace
or to compete with the original POPT library in any way.

<h2>Authors</h2>

<pkg_author name="Erik W. Troan" mail="ewt@redhat.com">
<pkg_author name="Ralf S. Engelschall" mail="rse@engelschall.com">

<h2>Status</h2>

<pkg_status
    name="popt" assign="rse"
    stable="none"    stable_date="none"
    unstable="none"  unstable_date="none"
	done=100>

<h2>Source</h2>

<pkg_files 
    cvs=$(CVS_ROOT_URL)/pkg/lib/popt/
    url=$(FTP_ROOT_URL)/pkg/lib/popt/
    directory=$(FTP_ROOT_DIR)/pkg/lib/popt/
    files="popt-*.tar.gz" 
	stable="none" unstable="none">
	
